USE [TimeSheet]
GO

/****** Object:  Table [dbo].[Timesheets]    Script Date: 4/24/2019 2:34:11 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Timesheets](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[EmployeeId] [int] NOT NULL,
	[TaskId] [int] NOT NULL,
	[TaskDate] [datetime] NOT NULL,
	[Hours] [int] NOT NULL,
 CONSTRAINT [PK_Timesheet] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Timesheets] ADD  CONSTRAINT [DF_Timesheets_Hours]  DEFAULT ((0)) FOR [Hours]
GO

